package stock;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Stock{

    //create an object for every line -> used in addToArrayFromFile();
    private static Product readOneProduct(String[] readProduct, String readedFile) {
        Product oneProduct = new Product();
        oneProduct.setSerialNumber(readProduct[0]);
        if(readProduct.length < 4) {
            if(readedFile.equals("icecream.csv")) oneProduct.setType("Icecream");
            else if(readedFile.equals("shampoo.csv")) oneProduct.setType("Shampoo");
            oneProduct.setQuality(Integer.parseInt(readProduct[1]));
            oneProduct.setProperty(readProduct[2]);
        }
        else if(readProduct.length == 4){
            oneProduct.setType(readProduct[1]);
            oneProduct.setQuality(Integer.parseInt(readProduct[2]));
            oneProduct.setProperty(readProduct[3]);
        }
        oneProduct.setPrice(oneProduct.getType(),oneProduct.getQuality());
        return oneProduct;
    }

    //read file with name csvFileName and create ArrayList with Product-s
    public static void addToArrayFromFile(ArrayList<Product> productArray, String csvFileName) {

        BufferedReader br;
        String line;
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFileName));
            line = br.readLine();
            while ((line = br.readLine()) != null) {

                String[] readedProduct = line.split(cvsSplitBy);
                productArray.add(readOneProduct(readedProduct,csvFileName));

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //sort all products by price (bubble sort)
    private static void sortArrayList(ArrayList<Product> sortArray) {

        boolean flag;

        do {
            flag = false;
            for(int i = 0; i < sortArray.size()-1; i++) {
                if(sortArray.get(i).getPrice()<sortArray.get(i+1).getPrice()) {
                    Product swap = sortArray.get(i);
                    sortArray.set(i,sortArray.get(i+1));
                    sortArray.set(i+1,swap);
                    flag = true;
                }
            }
        }while(flag);
    }

    //prints most expensive products and user chooses how many products to be printed
    public static void mostExpensive (ArrayList<Product> products, int numberOfMostExpensive) {
        sortArrayList(products);
        for (int i = 0; i < numberOfMostExpensive; i++) {
            System.out.println(products.get(i).getSerialNumber() + " " + products.get(i).getType() + " " +
                    products.get(i).getQuality() + " " + products.get(i).getProperty() + " "
                    + products.get(i).getPrice());
        }

    }

    //user chooses the type and the property for 3. from the menu
    public static int numberTypeWithProperty(ArrayList<Product> products, String productType, String productProperty) {
        int number = 0;
        for (Product pr : products) {
            if(pr.getType().equalsIgnoreCase(productType) &&
                    pr.getProperty().equalsIgnoreCase(productProperty)) number++;
        }
        return number;
    }

    public static void productsWithConcreteQuality(ArrayList<Product> products, int concreteQuality) {
        for (Product pr : products) {
            if(pr.getQuality() == concreteQuality)
                System.out.println(pr.getSerialNumber() + " " + pr.getType() + " " +
                    pr.getQuality() + " " + pr.getProperty() + " " + pr.getPrice());
        }
    }

    public static void serialNumberFromTo(ArrayList<Product> products, int from, int to) {

        for (Product pr : products) {
            if(Integer.parseInt(pr.getSerialNumber()) >= from && Integer.parseInt(pr.getSerialNumber()) <= to)
                System.out.println(pr.getSerialNumber() + " " + pr.getType() + " " +
                        pr.getQuality() + " " + pr.getProperty() + " " + pr.getPrice());
        }
    }
}
