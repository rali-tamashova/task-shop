package stock;

import java.util.ArrayList;
import java.util.Scanner;


public class Shop extends Stock{

    public static void main (String[] args) {

        ArrayList<Product> allProducts = new ArrayList<>();
        int userChoice;
        Scanner scan = new Scanner(System.in);


        addToArrayFromFile(allProducts,"bread_stock.csv");
        addToArrayFromFile(allProducts,"cheese_stock.csv");
        addToArrayFromFile(allProducts,"tomato_stock.csv");
        addToArrayFromFile(allProducts,"icecream.csv");
        addToArrayFromFile(allProducts,"shampoo.csv");

        System.out.println("\t\t\t" + "User Menu" + '\n' +
                "1. The most expensive products." + '\n' +
                "2. Number of all products." + '\n' +
                "3. Type of product with concrete property. " + '\n' +
                "4. All products with concrete quality. " + '\n' +
                "5. ALl products with serial number from... to... " + '\n' +
                "6. Exit.");


        System.out.println("Your choice: ");
        do {
            userChoice = scan.nextInt();

            if(userChoice == 6){
                System.out.print("Goodbye!");
                break;
            }
            switch (userChoice) {
                case 1: System.out.print("Enter a number: ");
                    int numExpensive = scan.nextInt();
                    if(numExpensive>allProducts.size()){
                        System.out.println("The products are only " + allProducts.size());
                        break;
                    }
                    mostExpensive(allProducts,numExpensive); break;

                case 2: System.out.println("All products are: " + allProducts.size()); break;

                case 3: System.out.print("Enter type of product (first letter to be capital): ");
                    scan.nextLine();
                    String typeProduct = scan.nextLine();
                    System.out.println("Enter property (first letter to be capital):  ");
                    String propertyProduct = scan.nextLine();
                    System.out.println("Number is: " +
                            numberTypeWithProperty(allProducts,typeProduct,propertyProduct)); break;

                case 4: System.out.println("Enter a quality: ");
                    int quality = scan.nextInt();
                    productsWithConcreteQuality(allProducts,quality); break;

                case 5: System.out.println("From");
                    int numberFrom = scan.nextInt();
                    System.out.println("To");
                    int numberTo = scan.nextInt();
                    serialNumberFromTo(allProducts,numberFrom,numberTo); break;

                default: System.out.println("No such case!");
            }

            System.out.println("Choose again: ");

        } while(true);


    }

}
