package stock;

import java.util.HashMap;
import java.util.Map;

public class Product {

    private String serialNumber;
    private String type;
//    private String quality;
    private int quality;
    private String property;
    private double price;
   /* private static double[] priceBread = {2.0, 1.5, 1.0};
    private static double[] priceCheese = {15.0, 12.0, 6.0};
    private static double[] priceTomato = {6.0, 4.5, 2.0};*/
    //private static Map<String,double[]> prices = new HashMap<>();

    private static Map<String,Map<Integer,Double>> prices = new HashMap<>();
    private static Map<Integer,Double> qualitiesBread = new HashMap<>();
    private static Map<Integer,Double> qualitiesCheese = new HashMap<>();
    private static Map<Integer,Double> qualitiesTomato = new HashMap<>();
    private static Map<Integer,Double> qualitiesIcecream = new HashMap<>();
    private static Map<Integer,Double> qualitiesShampoo = new HashMap<>();

    {
        qualitiesBread.put(1,2.0);
        qualitiesBread.put(2,1.5);
        qualitiesBread.put(3,1.0);

        qualitiesCheese.put(1,15.0);
        qualitiesCheese.put(2,12.0);
        qualitiesCheese.put(3,6.0);

        qualitiesTomato.put(1,6.0);
        qualitiesTomato.put(2,4.5);
        qualitiesTomato.put(3,2.0);

        qualitiesIcecream.put(1,15.0);
        qualitiesIcecream.put(2,12.0);
        qualitiesIcecream.put(3,6.0);

        qualitiesShampoo.put(1,12.0);
        qualitiesShampoo.put(2,8.0);
        qualitiesShampoo.put(3,4.5);

        prices.put("Bread",qualitiesBread);
        prices.put("Cheese",qualitiesCheese);
        prices.put("Tomato",qualitiesTomato);
        prices.put("Icecream",qualitiesIcecream);
        prices.put("Shampoo",qualitiesShampoo);

    }


    public void setSerialNumber(String number) {
        serialNumber = number;
    }
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setType(String newType) {
        type = newType;
    }
    public String getType() {
        return type;
    }

    /*public void setQuality(String newQuality) {
        quality = newQuality;
    }
    public String getQuality() {
        return quality;
    }*/

    public void setQuality(int newQuality) {
        quality = newQuality;
    }
    public int getQuality() {
        return quality;
    }

    public void setProperty(String newProperty) {
        property = newProperty;
    }
    public String getProperty() {
        return property;
    }

    //set price according to the type and the quality (already initialized HashMap)
    public void setPrice(String productType, int productQuality) {
        price = prices.get(productType).get(productQuality);
    }
    public double getPrice() {
        return price;
    }

}
